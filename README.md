README
======

*[The localizer code and releases have moved to GitHub!](https://github.com/pdedecker/Localizer)* No further development will take place on this repository.
